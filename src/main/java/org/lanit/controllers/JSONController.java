package org.lanit.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.lanit.models.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class JSONController {
    @PostMapping(path = "json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object request(@RequestBody Request request, @RequestParam String action) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Info info;
        long startTime = System.currentTimeMillis();
        UUID uuid = UUID.randomUUID();
        LocalDateTime dt = LocalDateTime.now();
        DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        String lastUpdate = dtFormatter.format(dt);

        Response updatedResponse = new Response();
        updatedResponse.setInfo(request.getInfo());
        updatedResponse.setLastUpdate(lastUpdate);
        updatedResponse.setUuid(String.valueOf(uuid));

        if ("add".equals(action)) {
            // ... код для добавления
            Add add = request.getAdd();
            List<TickersItem> tickers = request.getInfo().getTickers();
            boolean tickerFound = false;
            for (TickersItem ticker : tickers) {
                if (ticker.getTicker().equals(add.getName())) {
                    AlertsItem alertsItem = new AlertsItem();
                    alertsItem.setPercent(add.getPercent());
                    alertsItem.setTimeframe(add.getTimeFrame());
                    ticker.getAlerts().add(alertsItem);
                    request.setLastUpdate(lastUpdate);
                    request.setUuid(String.valueOf(uuid));
                    tickerFound = true;
                    break;
                }
            }
            if (!tickerFound) {
                TickersItem newTicker = new TickersItem();
                newTicker.setTicker(add.getName());
                List<AlertsItem> newAlerts = new ArrayList<>();
                AlertsItem alertsItem = new AlertsItem();
                alertsItem.setPercent(add.getPercent());
                alertsItem.setTimeframe(add.getTimeFrame());
                newAlerts.add(alertsItem);
                newTicker.setAlerts(newAlerts);
                tickers.add(newTicker);
                request.setLastUpdate(lastUpdate);
                request.setUuid(String.valueOf(uuid));
            }
        } else if ("delete".equals(action)) {
            Delete delete = request.getDelete();
            List<TickersItem> tickers = request.getInfo().getTickers();
            boolean removed = false;
            for (TickersItem ticker : tickers) {
                if (ticker.getTicker().equals(delete.getTickerName())) {
                    List<AlertsItem> alerts = ticker.getAlerts();
                    if (alerts.size() > delete.getAlertIndex()) {
                        alerts.remove(delete.getAlertIndex());
                        removed = true;
                        break;
                    } else {
                        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Передан некорректный индекс");
                    }
                }
            }
            if (removed) {
                request.setLastUpdate(lastUpdate);
                request.setUuid(String.valueOf(uuid));
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Передан некорректный тикер");
            }
        } else {
            return ResponseEntity.badRequest()
                    .header("content-type", "application/json")
                    .body("{\"message\": \"Передан некорректный action - ".concat(action).concat("\"}"));
        }

        return ResponseEntity.ok()
                .header("content-type", "application/json")
                .body(objectMapper.writeValueAsString(updatedResponse));
    }
}
